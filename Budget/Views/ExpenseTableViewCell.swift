//
//  PTGInputTableViewCell.swift
//  Budget
//
//  Created by Petar Gezenchov on 21/09/2016.
//  Copyright © 2016 Petar Gezenchov. All rights reserved.
//

import UIKit

class ExpenseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel?
    @IBOutlet weak var descriptionTextLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    
}
