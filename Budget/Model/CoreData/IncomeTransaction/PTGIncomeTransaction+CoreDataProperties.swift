//
//  PTGIncomeTransaction+CoreDataProperties.swift
//  Budget
//
//  Created by Petar Gezenchov on 05/01/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//
//

import Foundation
import CoreData


extension PTGIncomeTransaction {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PTGIncomeTransaction> {
        return NSFetchRequest<PTGIncomeTransaction>(entityName: "IncomeTransaction")
    }

    @NSManaged public var source: PTGIncomeGroup?

    @nonobjc public class func fetchRequestForAllIncomes(ascending: Bool) -> NSFetchRequest<PTGIncomeTransaction> {
        // TODO: Safer access of date
        let fetchRequest = NSFetchRequest<PTGIncomeTransaction>(entityName: "IncomeTransaction")
        let dateSortDescriptor = NSSortDescriptor(key: "date", ascending: ascending)
        fetchRequest.sortDescriptors = [dateSortDescriptor]
        return fetchRequest
    }
    
    @nonobjc public class func fetchRequestForPeriod(_ period: Period, ascending: Bool) -> NSFetchRequest<PTGIncomeTransaction> {
        // TODO: Safer access of date
        let fetchRequest = NSFetchRequest<PTGIncomeTransaction>(entityName: "IncomeTransaction")
        let dateSortDescriptor = NSSortDescriptor(key: "date", ascending: ascending)
        let sortSortDescriptor = NSSortDescriptor(key: "sort", ascending: true)
        fetchRequest.predicate = NSPredicate(format: "date >= %@ AND date <= %@", period.from as NSDate, period.to as NSDate)
        fetchRequest.sortDescriptors = [dateSortDescriptor, sortSortDescriptor]
        return fetchRequest
    }
    
    // TODO: Find out why fetchRequest() not called to avoid renaming the method to make it work
    
    @nonobjc public class func fetchRequestForDay(date: NSDate?) -> NSFetchRequest<PTGIncomeTransaction> {
        // TODO: Safer access of date
        let fetchRequest = NSFetchRequest<PTGIncomeTransaction>(entityName: "IncomeTransaction")
        fetchRequest.predicate = NSPredicate(format: "date == %@", date!.dateWithoutTime()! as NSDate)
        return fetchRequest
    }
}
