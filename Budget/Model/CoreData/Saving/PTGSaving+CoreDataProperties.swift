//
//  PTGSaving+CoreDataProperties.swift
//  Budget
//
//  Created by Petar Gezenchov on 05/01/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//
//

import Foundation
import CoreData


extension PTGSaving {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PTGSaving> {
        return NSFetchRequest<PTGSaving>(entityName: "Saving")
    }

    @NSManaged public var amount: Double
    @NSManaged public var name: String?
    @NSManaged public var sort: Int16

}
