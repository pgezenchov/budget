//
//  CoreDataManager.h
//  FantasticServices
//
//  Created by First Online DevMac 001 on 2/20/14.
//  Copyright (c) 2014 First Online Solutions Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef enum : NSUInteger
{
    FOSObjectSynced = 0,
    FOSObjectCreated,
    FOSObjectDeleted,
    FOSObjectModified
} FOSObjectSyncStatus;

@interface CoreDataManager : NSObject

+ (id)sharedInstance;
+ (NSManagedObjectContext*)masterContext;
+ (void)save;
+ (NSArray*)fetch:(NSFetchRequest*)fetchRequest;

- (void)registerForNotifications;

- (NSURL *)storeURL;
- (NSString *)storePath;
- (NSString *)databaseName;
- (NSString *)applicationDocumentsDirectory;

- (void)copyDatabaseToURL:(NSURL *)storeURL;
- (BOOL)backupSourceStoreAtURL:(NSURL *)sourceStoreURL movingDestinationStoreAtURL:(NSURL *)destinationStoreURL error:(NSError **)error;

- (NSManagedObjectContext *)newManagedObjectContext;
- (NSManagedObjectContext *)masterManagedObjectContext;
- (NSManagedObjectContext *)backgroundManagedObjectContext;

- (void)save;
- (void)saveMasterContext;
- (void)saveBackgroundContext;

- (BOOL)doesRequireMigrationWithError:(NSError **)error;

- (NSManagedObjectModel *)managedObjectModel;
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;

- (BOOL)progressivelyMigrateWithError:(NSError **)error;

@end

@interface CoreDataManager (Helper)

// For retrieval of objects
+ (NSMutableArray *)getObjectsForEntity:(NSString*)entityName withSortKey:(NSString*)sortKey andSortAscending:(BOOL)sortAscending andContext:(NSManagedObjectContext *)managedObjectContext;
+ (NSMutableArray *)searchObjectsForEntity:(NSString*)entityName withPredicate:(NSPredicate *)predicate andSortKey:(NSString*)sortKey andSortAscending:(BOOL)sortAscending andContext:(NSManagedObjectContext *)managedObjectContext;

// For deletion of objects
+ (BOOL)deleteAllObjectsForEntity:(NSString*)entityName withPredicate:(NSPredicate*)predicate andContext:(NSManagedObjectContext *)managedObjectContext;
+ (BOOL)deleteAllObjectsForEntity:(NSString*)entityName andContext:(NSManagedObjectContext *)managedObjectContext;

// For counting objects
+ (NSUInteger)countForEntity:(NSString *)entityName andContext:(NSManagedObjectContext *)managedObjectContext;
+ (NSUInteger)countForEntity:(NSString *)entityName withPredicate:(NSPredicate *)predicate andContext:(NSManagedObjectContext *)managedObjectContext;

+ (id)insertNewObjectForEntityForName:(NSString *)entityName inManagedObjectContext:(NSManagedObjectContext *)context;

+ (id)searchEntity:(NSString *)entity usingPredicate:(NSPredicate *)predicate sortAscending:(BOOL)sortAscending returnSignleObject:(BOOL)returnSingleObject;
+ (id)searchEntity:(NSString *)entity usingPredicate:(NSPredicate *)predicate sortAscending:(BOOL)sortAscending sortKey:(NSString *)sort returnSignleObject:(BOOL)returnSingleObject;

+ (NSString *)entityNameForClass:(NSString *)className;
+ (Class)classForEntityName:(NSString *)entityName;

+ (void)deleteAllObjectsInCoreData:(NSArray *)exceptions;

@end
