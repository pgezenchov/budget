//
//  PeriodTableViewCell.swift
//  Budget
//
//  Created by Petar Gezenchov on 24/08/2018.
//  Copyright © 2018 Petar Gezenchov. All rights reserved.
//

import UIKit

class PeriodTableViewCell: UITableViewCell {

    @IBOutlet weak var intervalLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!

}
