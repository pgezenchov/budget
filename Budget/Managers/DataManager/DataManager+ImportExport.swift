//
//  DataManager+ImportExport.swift
//  Budget
//
//  Created by Petar Gezenchov on 29/12/2018.
//  Copyright © 2018 Petar Gezenchov. All rights reserved.
//

import Foundation

extension DataManager {
    
    struct Constants {
        static let exportJSONFileName = "expenses.json"
        static let exportTextFileName = "expenses.txt"
        static let importJSONFileName = "expenses"
        static let importJSONFileExtension = "json"
    }
    
    /// Writes database into a file in JSON format
    ///
    /// File is created in Documents directory
    /// File path is printed in console
    static func exportDatabaseAsJSONInFile() {
        var jsonString: String?
        
        print("Started export of expenses...")
        
        // Read all expenses and convert into JSON string
        do {
            // Read all expenses
            let fetchRequest = PTGExpenseTransaction.fetchRequestForAllExpenses(ascending: true)
            let expenses = try (CoreDataManager.sharedInstance() as! CoreDataManager).masterManagedObjectContext().fetch(fetchRequest)
            var expensesDictionaries: [[String: Any]] = []
            
            print("Read a total of \(expenses.count) expenses")
            print("Started creating dictionaries...")
            
            // Create a dictionary for every expense
            for case let expense in expenses {
                var expensesDictionary: [String: Any] = [:]
                
                expensesDictionary["amount"] = expense.amount
                expensesDictionary["description"] = expense.descriptionText ?? "Unknown"
                expensesDictionary["categoryName"] = expense.category?.name ?? "Unknown"
                expensesDictionary["date"] = expense.dateString
                expensesDictionary["sort"] = expense.sort
                
                // Add expense dictionaries in an array
                expensesDictionaries.append(expensesDictionary)
            }
            
            print("... dictionaries created")
            print("Converting dictionaries to JSON...")
            
            do {
                // Convert array to JSON string
                let jsonData = try JSONSerialization.data(withJSONObject: expensesDictionaries, options: .prettyPrinted)
                guard let aJsonString = String.init(data: jsonData, encoding: .utf8) else {
                    print("Error converting expense to JSON string")
                    return;
                }
                
                jsonString = aJsonString
            } catch {
                print(error.localizedDescription)
            }
        }
        catch { /* Catch read errors here */
            print("Fetch failed!")
        }
        
        print("... converted to JSON")
        print("Writing JSON to file...")
        
        // Write JSON to a file
        let file = Constants.exportJSONFileName
        let text = jsonString!
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            // Path to file in Documents directory
            let fileURL = dir.appendingPathComponent(file)
            
            // Write text to file
            do {
                try text.write(to: fileURL, atomically: false, encoding: .utf8)
                print("Database exported in JSON format in: \(fileURL)")
            }
            catch {/* error handling here */
                print("Failed to export database in JSON")
            }
        }
    }
    
    /// Import expenses from JSON
    ///
    /// Reads from a file in bundle named importJSONFileName+importJSONFileExtension and creates expenses in database
    static func importExpensesFromJSONFile() {
        var expensesDictionaries: [[String: Any]] = []
        
        print("Started import of expenses...")
        
        // Path to JSON file in Bundle
        guard let fileURL = Bundle.main.url(forResource: Constants.importJSONFileName, withExtension: Constants.importJSONFileExtension) else {
            print("Import file \(Constants.importJSONFileName)\(Constants.importJSONFileExtension) not found in bundle. No file to import from.")
            return;
        }
        
        print("Reading JSON expenses from \(Constants.importJSONFileName).\(Constants.importJSONFileExtension)...")
        
        // Read JSON file from Bundle as text
        do {
            let text = try String(contentsOf: fileURL, encoding: .utf8)
            
            // Convert text to JSON
            let data = text.data(using: .utf8)!
            
            do {
                if let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [Dictionary<String,Any>]
                {
                    expensesDictionaries = jsonArray
                } else {
                    print("Bad json in \(Constants.importJSONFileName)\(Constants.importJSONFileExtension)")
                }
            } catch let error as NSError {
                print(error)
            }
        }
        catch {/* error handling here */}
        
        let readJSONRecords = expensesDictionaries.count
        
        print("Read a total of \(readJSONRecords) expenses")
        print("Started creating expenses from JSON...")
        
        // Create expenses from JSON
        
        // Load categories
        let fetchRequest = NSFetchRequest<PTGExpenseGroup>(entityName: "ExpenseGroup")
        let nameSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [nameSortDescriptor]
        let frcCategories: NSFetchedResultsController<PTGExpenseGroup>! = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataManager.masterContext()!, sectionNameKeyPath:nil, cacheName: nil)
        
        let context = CoreDataManager.masterContext()
        context?.performAndWait {
            do {
                try frcCategories.performFetch()
            } catch {
                fatalError("Failed to fetch entities: \(error)")
            }
        }
        
        let categories = frcCategories.fetchedObjects
        
        var createdExpensesCount = 0
        
        for expenseDictionary in expensesDictionaries {
            let expense = PTGExpenseTransaction.createExpenseTransaction()
            
            // Extract expense data from JSON
            let categoryName = expenseDictionary["categoryName"] as! String
            let category = categories!.filter { $0.name! == categoryName }.first
            let date = NSDate.fromExpenseDateString(expenseDictionary["date"] as? String) as NSDate
            
            // Set expense data
            expense.amount = (expenseDictionary["amount"] as! NSNumber).doubleValue
            expense.descriptionText = expenseDictionary["description"] as? String
            expense.sort = (expenseDictionary["sort"] as! NSNumber).int16Value
            expense.date = date
            expense.category = category
            
            // Save expense
            CoreDataManager.save()
            
            createdExpensesCount += 1
            
            print("Created \(createdExpensesCount)/\(readJSONRecords) expenses")
        }
        
        print("Expenses successfully imported!")
    }
    
    /// Writes a period into a file in plain text format
    ///
    /// File is created in Documents directory
    /// File path is printed in console
    static func exportPeriodAsTextInFile(_ period: Period) {
        var textString = ""
        
        print("Started export of expenses for period  \(period.from) to \(period.to)...")
        
        // Read all expenses for period and convert to string
        do {
            // Read expenses
            let fetchRequest = PTGExpenseTransaction.fetchRequestForPeriod(period, ascending: true)
            let context = CoreDataManager.masterContext()
            
            var expenses: [PTGExpenseTransaction] = []
            
            context?.performAndWait {
                do {
                    expenses = try (CoreDataManager.sharedInstance() as! CoreDataManager).masterManagedObjectContext().fetch(fetchRequest)
                } catch {
                    fatalError("Failed to fetch entities: \(error)")
                }
            }
            
            print("Read a total of \(expenses.count) expenses")
            print("Started creating text to write in file...")
            
            // Create a string for days
            // Add a day only once. If there is another expense for the same day replace it with an empty string
            let calendar = Calendar.current
            let dates = expenses.map { String(calendar.component(.day, from: $0.date! as Date)) }
            var parsedDates:[String] = []
            
            for date in dates {
                if !parsedDates.contains(date) {
                    parsedDates.append(date)
                }
                else {
                    parsedDates.append("")
                }
            }
            
            // Convert ammount, descriptions and category names to array
            let amounts = expenses.map { String($0.amount) }
            let descriptions = expenses.map { $0.descriptionText ?? "Unknown" }
            let categoryNames = expenses.map { $0.category?.name ?? "Unknown" }
            
            // Convert arrays to strings with each element on a new row
            let datesText = parsedDates.joined(separator: "\n")
            let amountsText = amounts.joined(separator: "\n")
            let descriptionsText = descriptions.joined(separator: "\n")
            let categoriesText = categoryNames.joined(separator: "\n")
            
            // Append strings into one
            textString.append(contentsOf: datesText)
            textString.append(contentsOf: "\n\n")
            
            textString.append(contentsOf: amountsText)
            textString.append(contentsOf: "\n\n")
            
            textString.append(contentsOf: descriptionsText)
            textString.append(contentsOf: "\n\n")
            
            textString.append(contentsOf: categoriesText)
            textString.append(contentsOf: "\n\n")
            
            print("... text created")
            print(textString)
        }
        catch { /* Catch read errors here */
            print("Fetch failed!")
        }
        
        print("Writing text to file...")
        
        // Write text to a file
        let file = Constants.exportTextFileName
        let text = textString
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            // Path to file in Documents directory
            let fileURL = dir.appendingPathComponent(file)
            
            // Write text to file
            do {
                try text.write(to: fileURL, atomically: false, encoding: .utf8)
                print("Database exported in JSON format in: \(fileURL)")
            }
            catch {/* error handling here */
                print("Failed to export database in JSON")
            }
        }
    }
    
}
