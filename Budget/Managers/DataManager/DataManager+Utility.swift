//
//  DataManager+Utility.swift
//  Budget
//
//  Created by Petar Gezenchov on 24/08/2018.
//  Copyright © 2018 Petar Gezenchov. All rights reserved.
//

import Foundation

extension DataManager {
    static func printDatabaseLocalPath () {
        print("Documents:" + NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
    }
    
    static func initExpenseGroups() {
        do {
            let categories = try (CoreDataManager.sharedInstance() as! CoreDataManager).masterManagedObjectContext().fetch(PTGExpenseGroup.fetchRequest()) as! [PTGExpenseGroup]
            if  categories.count == 0 {
                let staticCategories = ["обяд офис", "храна", "битови", "лични разходи", "периодични", "гориво", "сметки" , "извънредни"]
                for (index, element) in staticCategories.enumerated() {
                    _ = PTGExpenseGroup.createExpenseGroup(name: element, sort: Int16(index))
                }
                
                // Save expense groups
                CoreDataManager.save()
            }
        } catch {
            fatalError("Failed to fetch category: \(error)")
        }
    }
    
    static func initIncomeSources() {
        do {
            let sources = try (CoreDataManager.sharedInstance() as! CoreDataManager).masterManagedObjectContext().fetch(PTGIncomeGroup.fetchRequest()) as! [PTGIncomeGroup]
            if  sources.count == 0 {
                let staticSources = ["заплата", "ваучери за храна", "бонус", "подарък", "фрийланс", "други"]
                for (index, element) in staticSources.enumerated() {
                    _ = PTGIncomeGroup.createIncomeGroup(name: element, sort: Int16(index))
                }
                
                // Save expense groups
                CoreDataManager.save()
            }
        } catch {
            fatalError("Failed to fetch category: \(error)")
        }
    }
}
