//
//  HistoryViewController.swift
//  Budget
//
//  Created by Petar Gezenchov on 24/08/2018.
//  Copyright © 2018 Petar Gezenchov. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    let allPeriodsAndTotatls = DataManager.allPeriodsAndTotals()
    
    // MARK: View Controller Lifecycle
    
    override func viewDidLoad() {
        let averageValue = String(format: "%.2f", DataManager.averageTotalExpensesForAllPeriods())
        self.navigationItem.title = "Average: \(averageValue)"
    }
    
    // MARK: Table View Data Source methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allPeriodsAndTotatls.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PTGPeriodTableViewCell", for: indexPath) as! PeriodTableViewCell
        
        let object = allPeriodsAndTotatls[indexPath.row]
        
        // Configure the cell with data from the object.
        cell.intervalLabel?.text = "\(object.period.from.dateAndMonthString)\n\n\(object.period.to.dateAndMonthString)"
        cell.totalLabel?.text = String(format: "%.2f", object.total)
        
        return cell
    }
    
    // MARK: Segue methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if  let destination = segue.destination as? ExpensesListViewController,
            let selectedRowIndex = self.tableView.indexPathForSelectedRow
        {
            let selecetdPeriod = allPeriodsAndTotatls[selectedRowIndex.row].period
            destination.period = selecetdPeriod
            destination.showsHistory = true
            self.tableView.deselectRow(at: selectedRowIndex, animated: true)
            
            let backItem = UIBarButtonItem()
            backItem.title = "\(selecetdPeriod.from.monthString) - \(selecetdPeriod.to.monthString) \(selecetdPeriod.to.yearString)"
            navigationItem.backBarButtonItem = backItem
        }
    }
    
}
