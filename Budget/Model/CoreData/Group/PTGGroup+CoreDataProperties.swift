//
//  PTGGroup+CoreDataProperties.swift
//  Budget
//
//  Created by Petar Gezenchov on 05/01/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//
//

import Foundation
import CoreData


extension PTGGroup {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PTGGroup> {
        return NSFetchRequest<PTGGroup>(entityName: "Group")
    }

    @NSManaged public var name: String?
    @NSManaged public var sort: Int16

}
