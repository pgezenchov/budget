//
//  PTGIncomeGroup+CoreDataProperties.swift
//  Budget
//
//  Created by Petar Gezenchov on 05/01/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//
//

import Foundation
import CoreData


extension PTGIncomeGroup {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PTGIncomeGroup> {
        return NSFetchRequest<PTGIncomeGroup>(entityName: "IncomeGroup")
    }

    @NSManaged public var incomes: NSSet?

}

// MARK: Generated accessors for incomes
extension PTGIncomeGroup {

    @objc(addIncomesObject:)
    @NSManaged public func addToIncomes(_ value: PTGIncomeTransaction)

    @objc(removeIncomesObject:)
    @NSManaged public func removeFromIncomes(_ value: PTGIncomeTransaction)

    @objc(addIncomes:)
    @NSManaged public func addToIncomes(_ values: NSSet)

    @objc(removeIncomes:)
    @NSManaged public func removeFromIncomes(_ values: NSSet)

}
