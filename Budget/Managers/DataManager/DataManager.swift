//
//  DataManager.swift
//  Budget
//
//  Created by Petar Gezenchov on 21/01/2018.
//  Copyright © 2018 Petar Gezenchov. All rights reserved.
//

import UIKit

class DataManager: NSObject {
    private static var allPeriods: [Period] = DataManager.loadAllPeriods()
    private static var preloadedAllPeriodsAndTotals: [(Period, Double)] = DataManager.loadAllPeriodsAndTotals()
    
    static func totalExpensesForCurrentPeriod() -> Double {
        return totalExpensesForPeriod(Period.current())
    }
    
    static func totalExpensesForPeriod(_ period: Period) -> Double {
        do {
            let expenses = try (CoreDataManager.sharedInstance() as! CoreDataManager).masterManagedObjectContext().fetch(PTGExpenseTransaction.fetchRequestForPeriod(period, ascending: false))
            var totalExpenses: Double = 0
            for expense in expenses {
                totalExpenses += expense.amount
            }
            return totalExpenses
        } catch {
            fatalError("Failed to fetch category: \(error)")
        }
        
        return 0
    }
    
    static func totalSavings() -> Double {
        var totalIncomes: Double = 0
        var totalExpenses: Double = 0
        
        do {
            let incomes = try (CoreDataManager.sharedInstance() as! CoreDataManager).masterManagedObjectContext().fetch(PTGIncomeTransaction.fetchRequestForAllIncomes(ascending: true))
            for income in incomes {
                totalIncomes += income.amount
            }
        } catch {
            fatalError("Failed to fetch category: \(error)")
        }
        
        do {
            let expenses = try (CoreDataManager.sharedInstance() as! CoreDataManager).masterManagedObjectContext().fetch(PTGExpenseTransaction.fetchRequestForAllExpenses(ascending: true))
            for expense in expenses {
                totalExpenses += expense.amount
            }
        } catch {
            fatalError("Failed to fetch category: \(error)")
        }
        
        return totalIncomes - totalExpenses
    }
    
    static func averageTotalExpensesForAllPeriods() -> Double {
        let totals = allPeriodsAndTotals().map { $0.total }
        
        let average = totals.reduce(0.0) {
            return $0 + $1/Double(totals.count)
        }
        
        return average
    }
    
    static func loadAllPeriods() -> [Period] {
//        DispatchQueue.main.sync {
            do {
                let allExpenses = try (CoreDataManager.sharedInstance() as! CoreDataManager).masterManagedObjectContext().fetch(PTGExpenseTransaction.fetchRequestForAllExpenses(ascending: true))
                
                guard let firstExpense = allExpenses.first, let firstExpenseDate = firstExpense.date else {
                    return []
                }
                
                return Period.allPeriodsAfterDate(firstExpenseDate as Date)
            } catch {
                fatalError("Failed to fetch category: \(error)")
            }
            
            return []
//        }
    }
    
    static func loadAllPeriodsAndTotals() -> [(period: Period, total: Double)] {
        var periodsAndTotals: [(Period, Double)] = []
        
        for period in allPeriods.reversed() {
            periodsAndTotals.append((period, totalExpensesForPeriod(period)))
        }
        return periodsAndTotals
    }
    
    static func allPeriodsAndTotals() -> [(period: Period, total: Double)] {
        return preloadedAllPeriodsAndTotals
    }
}
