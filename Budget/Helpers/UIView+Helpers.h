//
//  UIView+Helpers.h
//  BFantastic
//
//  Created by Adrian on 3/31/15.
//  Copyright (c) 2015 com.firstonlinesolutions. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    FOSUIViewBorderNone     = 0,
    FOSUIViewBorderLeft     = 1 << 0,
    FOSUIViewBorderRight    = 1 << 1,
    FOSUIViewBorderTop      = 1 << 2,
    FOSUIViewBorderBottom   = 1 << 3
} FOSUIViewBorder;

@interface UIView (Helpers)

- (void)gradientWithStartColor:(UIColor*)startColor andEndColor:(UIColor*) endColor;

- (void)addBorder:(FOSUIViewBorder)border;

- (void)addBorder:(FOSUIViewBorder)border color:(UIColor*)color;

/**
 *  Creates a view from nib.
 *
 *  @param nibName Name of the nib's fil.
 *
 *  @return View instance.
 */
+ (UIView *)getViewFromNibWithNibName:(NSString *)nibName;

/**
 *  Draws corners and borders to a view.
 *
 *  @param color  Border color.
 *  @param width  Border width.
 *  @param radius Corner radius.
 */
- (void)addRoundedCornersWithColor:(UIColor *)color
                   withBorderWidth:(CGFloat)width
                  withCornerRadius:(CGFloat)radius;

/**
 *  Performs shake animation on a view.
 */
- (void)shake;

- (id)findFirstResponder;

@end
