//
//  NSDateC.h
//  BFantastic
//
//  Created by Petar Gezenchov on 15/03/2015.
//  Copyright (c) 2015 com.firstonlinesolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Helpers)

+ (NSDate *)fromExpenseDateString:(NSString *)expenseDateString;

- (NSDate *)dateWithoutTime;

- (NSString *)localTimeString;

@end
