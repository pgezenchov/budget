//
//  Period.swift
//  Budget
//
//  Created by Petar Gezenchov on 11/02/2018.
//  Copyright © 2018 Petar Gezenchov. All rights reserved.
//

import UIKit

public class Period {
    var from: Date
    var to: Date
    
    public init(from fromDate: Date, to toDate: Date) {
        self.from = fromDate
        self.to = toDate
    }
    
    public convenience init(date: Date) {
        self.init(from: Period.periodStartDateForDate(date), to: Period.periodEndDateForDate(date))
    }
    
    static func next() -> Period {
        return Period(from: Date.aMonthFrom(Period.currentPeriodStartDate()), to: Date.aMonthFrom(Period.currentPeriodEndDate()))
    }
    
    static func current() -> Period {
        return Period(from: Period.currentPeriodStartDate(), to: Period.currentPeriodEndDate())
    }
    
    static func last() -> Period {
        return Period(from: Date.aMonthBefore(Period.currentPeriodStartDate()), to: Date.aMonthBefore(Period.currentPeriodEndDate()))
    }
    
    static func beforeLast() -> Period {
        let fromDate = Date.aMonthBefore(Period.currentPeriodStartDate())
        let toDate = Date.aMonthBefore(Period.currentPeriodEndDate())
        return Period(from: Date.aMonthBefore(fromDate), to: Date.aMonthBefore(toDate))
    }
    
    static func beforeBeforeLast() -> Period {
        var fromDate = Date.aMonthBefore(Period.currentPeriodStartDate())
        var toDate = Date.aMonthBefore(Period.currentPeriodEndDate())
        
        fromDate = Date.aMonthBefore(fromDate)
        toDate = Date.aMonthBefore(toDate)
        
        return Period(from: Date.aMonthBefore(fromDate), to: Date.aMonthBefore(toDate))
    }
    
    static func allPeriodsAfterDate(_ date: Date) -> [Period] {
        var aDate = date
        let now = Date()
        var periods: [Period] = []
        
        while aDate < now {
            let firstPeriodStartDate = periodStartDateForDate(aDate)
            let firstPeriodLastDate = periodEndDateForDate(aDate)
            
            periods.append(Period(from: firstPeriodStartDate, to: firstPeriodLastDate))
            
            aDate = Date.aMonthFrom(aDate)
        }
        
        return periods
    }
    
    static func startDayOfMonth() -> Int {
        // TODO: Read from configuration
        return 16
    }
    
    private static func currentPeriodStartDate() -> Date {
        return Date.periodStartDateForDate(Date(), periodStart: Period.startDayOfMonth())
    }
    
    private static func periodStartDateForDate(_ date: Date) -> Date {
        return Date.periodStartDateForDate(date, periodStart: Period.startDayOfMonth())
    }
    
    private static func currentPeriodEndDate() -> Date {
        return periodEndDateForDate(Date())
    }
    
    private static func periodEndDateForDate(_ date: Date) -> Date {
        let periodEndDay = Period.startDayOfMonth() - 1
        let periodStartDate = Date.periodStartDateForDate(date, periodStart: periodEndDay)
        let periodEndDate = Date.aMonthFrom(periodStartDate)
        let periodEndDateEndOfDay = Date.endOfDay(periodEndDate)
        
        return periodEndDateEndOfDay
    }
}
