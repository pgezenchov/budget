//
//  AddExpenseViewController.swift
//  Budget
//
//  Created by Petar Gezenchov on 22/10/2016.
//  Copyright © 2016 Petar Gezenchov. All rights reserved.
//

import UIKit

enum TransactionType {
    case expense
    case income
}

// TODO: Think of way to share expense view controller with income
class ExpenseViewController: UIViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate {
    
    // MARK: Public variables
    var editingTransaction: PTGTransaction?
    var editingExpense: PTGExpenseTransaction? {
        get {
            return self.editingTransaction as? PTGExpenseTransaction
        }
    }
    var editingIncome: PTGIncomeTransaction? {
        get {
            return self.editingTransaction as? PTGIncomeTransaction
        }
    }
    var transactionType: TransactionType = .expense
    
    // MARK: IBOutlets
    
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var categoryPickerView: UIPickerView!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var datePickerContainerView: UIView!
    @IBOutlet weak var todayButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var saveButton: UIButton!
    
    // MARK: Private variables
    
    private var frcCategories: NSFetchedResultsController<PTGGroup>! {
        get {
            switch self.transactionType {
            case .expense:
                return self.frcExpenseGroups as? NSFetchedResultsController<PTGGroup>
            case .income:
                return self.frcIncomeGroups as? NSFetchedResultsController<PTGGroup>
            }
        }
    }
    private var frcExpenseGroups: NSFetchedResultsController<PTGExpenseGroup>!
    private var frcIncomeGroups: NSFetchedResultsController<PTGIncomeGroup>!
    private var selectedCategoryIndex: Int?
    
    // MARK: View life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        amountTextField.becomeFirstResponder()
        updateDateButton(date: datePicker.date)
        
        // Round corners of save button
        saveButton.addRoundedCorners(with: nil, withBorderWidth: 0, withCornerRadius: 30.0)
        
        // Load expense groups
        let context = CoreDataManager.masterContext()
        let fetchRequest = NSFetchRequest<PTGExpenseGroup>(entityName: "ExpenseGroup")
        // Configure the request's entity, and optionally its predicate
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "sort", ascending: true)]
        frcExpenseGroups = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context!, sectionNameKeyPath:nil, cacheName: nil)
        
        context?.performAndWait {
            do {
                try frcExpenseGroups.performFetch()
            } catch {
                fatalError("Failed to fetch entities: \(error)")
            }
        }
        
        // Load income groups
        
        let incomeFetchRequest = NSFetchRequest<PTGIncomeGroup>(entityName: "IncomeGroup")
        // Configure the request's entity, and optionally its predicate
        incomeFetchRequest.sortDescriptors = [NSSortDescriptor(key: "sort", ascending: true)]
        frcIncomeGroups = NSFetchedResultsController(fetchRequest: incomeFetchRequest, managedObjectContext: context!, sectionNameKeyPath:nil, cacheName: nil)
        
        context?.performAndWait {
            do {
                try frcIncomeGroups.performFetch()
            } catch {
                fatalError("Failed to fetch entities: \(error)")
            }
        }
        
        if editingTransaction != nil {
            loadEditingExpenseInController()
        }
    }
    
    // MARK: UIPickerViewDataSource methods
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        if let frc = frcCategories {
            return frc.sections!.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // TODO: Handle if guard statements fail
        guard let sections = frcCategories?.sections else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[0]
        
        return sectionInfo.numberOfObjects
    }
    
    // MARK: UIPickerViewDelegate methods
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let category = categoryAtIndex(index: row)
        return category?.name ?? "unknown"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        updateSelectedCategory()
    }
    
    // MARK: UITextFieldDelegate methods
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if  textField == categoryTextField {
            updateSelectedCategory()
            self.view.endEditing(true)
            self.categoryPickerView.fadeIn()
            return false
        }
        
        return true
    }
    
    // MARK: IBAction methods
    
    @IBAction func dateButtonTapped(_ sender: AnyObject) {
        self.view.endEditing(true)
        self.datePickerContainerView.fadeIn()
    }
    
    @IBAction func todayTapped(_ sender: AnyObject) {
        let today = Date()
        updateDateButton(date: today)
        datePicker.setDate(today, animated: true)
        
        self.todayButton.fadeOut()
    }
    
    @IBAction func datePickerChanged(_ sender: AnyObject) {
        updateDateButton(date: datePicker.date)
    }
    
    @IBAction func backgroundTapped(_ sender: AnyObject) {
        self.view.endEditing(true)
        self.datePickerContainerView.fadeOut()
        self.categoryPickerView.fadeOut()
    }
    
    @IBAction func saveButtonTapped(_ sender: AnyObject) {
        var descriptionText = ""
        let formatter = NumberFormatter()
        formatter.decimalSeparator = NSLocale.current.decimalSeparator
        let amount = formatter.number(from:amountTextField.text!)
        
        // TODO: Replce checks with guard statements
        
        // Check amount input
        if amount == nil ||
            amount!.doubleValue <= 0 {
            let alert = UIAlertController(title: "Error", message: "You need to enter amount!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        // Check description input
        if let descriptionTextValue = descriptionTextField.text {
            if !descriptionTextValue.isEmpty {
                descriptionText = descriptionTextValue
            }
            else {
                // alert
                let alert = UIAlertController(title: "Error", message: "You need to enter description text!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }
        }
        
        // Check category input
        if selectedCategoryIndex == nil {
                // alert
            let alert = UIAlertController(title: "Error", message: "You need to enter category!", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        var category: PTGGroup? = nil
        if let selectedIndex = selectedCategoryIndex {
            category = categoryAtIndex(index: selectedIndex)
        }
        
        // Create expense or edit existing expense
        var expense: PTGTransaction!
        if let value = editingTransaction {
            expense = value
        }
        else {
            switch (transactionType) {
            case .expense:
                expense = PTGExpenseTransaction.createExpenseTransaction()
            case .income:
                expense = PTGIncomeTransaction.createIncomeTransaction()
            }
        }
        expense.amount = (amount?.doubleValue)!
        expense.descriptionText = descriptionText
        expense.date = datePicker.date as NSDate?
        
        switch (transactionType) {
        case .expense:
            (expense as? PTGExpenseTransaction)?.category = (categoryAtIndex(index: selectedCategoryIndex ?? 0) as? PTGExpenseGroup)
        case .income:
            (expense as! PTGIncomeTransaction).source = (categoryAtIndex(index: selectedCategoryIndex ?? 0) as? PTGIncomeGroup)
        }
        
        CoreDataManager.save()
        
        dismiss(animated: true, completion: {})
    }
    
    // MARK: Private methods
    
    func loadEditingExpenseInController() {
        amountTextField.text = editingExpense!.amountString
        descriptionTextField.text = editingExpense!.descriptionText
        if let category = editingExpense?.category {
            categoryPickerView.selectRow(frcCategories.indexPath(forObject: category)!.row, inComponent: 0, animated: true)
            updateSelectedCategory()
        }
        datePicker.setDate(editingExpense!.date as Date? ?? Date(), animated: true)
        updateDateButton(date: datePicker.date)
        saveButton.setTitle("Update", for: .normal)
    }
    
    func updateDateButton(date: Date) {
        // Hide or show "Set today" button if today selected or unselected from picker
        Calendar.current.isDateInToday(datePicker.date) ?
            self.todayButton.fadeOut():
            self.todayButton.fadeIn()
        
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.timeStyle = .none
        let dateString = formatter.string(from: date)
        
        dateButton.setTitle(dateString, for: .normal)
    }
    
    func categoryAtIndex(index: Int) -> PTGGroup? {
        guard let object = frcCategories?.object(at: IndexPath(row: index, section: 0)) else {
            fatalError("Attempt to configure cell without a managed object")
        }
        
        let category = object
        
        return category
    }
    
    func updateSelectedCategory(){
        let pickerSelectedIndex = categoryPickerView.selectedRow(inComponent: 0)
        let category = categoryAtIndex(index: pickerSelectedIndex)
        selectedCategoryIndex = pickerSelectedIndex
        categoryTextField.text = category?.name ?? "unknown"
    }

}
