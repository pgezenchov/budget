//
//  FirstViewController.swift
//  Budget
//
//  Created by Petar Gezenchov on 19/09/2016.
//  Copyright © 2016 Petar Gezenchov. All rights reserved.
//

import UIKit

class ExpensesListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    var period = Period.current()
    var showsHistory = false
    
    private var frcExpenses: NSFetchedResultsController<PTGExpenseTransaction>!
    private var editingIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // TODO: Handle if context can't be unwrapped here and in AddExpenseViewController.swift frcCategories
        // TODO: Make frcExpenses not force unwrapped and handle all across code here and in AddExpenseViewController.swift frcCategories
        // TODO: Check if fetchRequest can be nil and handle case here and in AddExpenseViewController.swift frcCategories
        
        let context = CoreDataManager.masterContext()
        let fetchRequest = PTGExpenseTransaction.fetchRequestForPeriod(self.period, ascending: false)
        frcExpenses = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context!, sectionNameKeyPath: "date.dateWithoutTime", cacheName: nil)
        reloadExpensesData()
        
        if showsHistory {
            self.navigationItem.leftBarButtonItem = nil
            self.navigationItem.rightBarButtonItem!.title = "Breakdown"
        }
        else {
            performSegue(withIdentifier: "NewExpenseSegue", sender: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadExpensesTable()
    }
    
    // MARK: UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // TODO: Handle if guard statements fail
        guard let sections = frcExpenses?.sections else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let frc = frcExpenses {
            return frc.sections!.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let sectionInfo = frcExpenses?.sections?[section] else {
            return nil
        }
        
        // Convert string section name to date
        let dateString = sectionInfo.name
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let dateFromString = dateFormatter.date(from: dateString)
        
        // Convert the date back to string with desired display format
        dateFormatter.dateFormat = "dd MMMM"
        let stringDate: String = dateFormatter.string(from: dateFromString!)
        
        return stringDate
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PTGExpenseTableViewCell", for: indexPath) as! ExpenseTableViewCell
        
        guard let object = frcExpenses?.object(at: indexPath) else {
            fatalError("Attempt to configure cell without a managed object")
        }
        
        // Configure the cell with data from the managed object.
        let expense = object
        
        // TODO: check if some additional checks should be made for optional UI elements before setting their value
        cell.amountLabel?.text = expense.amountString
        cell.descriptionTextLabel?.text = expense.descriptionText
        cell.dateLabel?.text = expense.dateString
        cell.categoryLabel.text = expense.category?.name ?? "unknown"
        
        return cell
    }
    
    // MARK: UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if sourceIndexPath.row != destinationIndexPath.row {
            guard let sections = frcExpenses?.sections else {
                fatalError("No sections in fetchedResultsController")
            }
            let sectionInfo = sections[sourceIndexPath.section]
            
            guard var objects = sectionInfo.objects else {
                fatalError("Attempt to configure cell without a managed object")
            }
            let expense = objects.remove(at: sourceIndexPath.row)
            objects.insert(expense, at: destinationIndexPath.row)
            for (index, element) in objects.enumerated() {
                (element as! PTGExpenseTransaction).sort = Int16(index)
            }
            CoreDataManager.save()
            reloadExpensesData()
        }
    }
    
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        if sourceIndexPath.section != proposedDestinationIndexPath.section {
            return sourceIndexPath
        }
        
        return proposedDestinationIndexPath
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            guard let object = self.frcExpenses?.object(at: indexPath) else {
                fatalError("Attempt to configure cell without a managed object")
            }
            
            // Check if last object in section will be deleted to remove section later
            let lastObjectInSection = self.frcExpenses.sections![indexPath.section].numberOfObjects == 1
            
            // Delete the row from the data source
            CoreDataManager.masterContext().delete(object)
            CoreDataManager.save()
            self.reloadExpensesData()
            
            // Remove the section if all records are removed
            if lastObjectInSection {
                tableView.deleteSections(IndexSet(integer: 0), with: .fade)
            }
            else {
                // Delete the row in table animated
                tableView.deleteRows(at: [indexPath as IndexPath], with: .fade)
            }
        }
        
        let edit = UITableViewRowAction(style: .default, title: "Edit") { (action, indexPath) in
            // edit item at indexPath
            // Show expense screen to edit the record
            self.editingIndexPath = indexPath
            self.performSegue(withIdentifier: "EditExpenseSegue", sender: nil)
        }
        edit.backgroundColor = UIColor.lightGray
        
        return [delete, edit]
    }
    
    // MARK: Segue methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // TODO: Think of a better way to set transaction type
        if (segue.identifier == "EditExpenseSegue") {
            if let indexPath = editingIndexPath {
                // edit item at indexPath
                guard let object = self.frcExpenses?.object(at: indexPath) else {
                    fatalError("Attempt to configure cell without a managed object")
                }
                let expenseViewController = segue.destination as! ExpenseViewController
                expenseViewController.editingTransaction = object
                expenseViewController.transactionType = .expense
                editingIndexPath = nil
            }
        }
        else if (segue.identifier == "NewExpenseSegue") {
            let expenseViewController = segue.destination as! ExpenseViewController
            expenseViewController.transactionType = .expense
        }
    }
    
    
    // MARK: IBActions
    
    @IBAction func editButtonPressed(_ sender: Any) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        (sender as! UIBarButtonItem).title = tableView.isEditing ? "Done" : "Edit"
    }
    
    @IBAction func rightButtonPressed(_ sender: Any) {
        if showsHistory {
            performSegue(withIdentifier: "BreakdownSegue", sender: nil)
        }
        else {
            performSegue(withIdentifier: "NewExpenseSegue", sender: nil)
        }
    }
    
    // MARK: Private methods
    
    func reloadExpensesTable() {
        reloadExpensesData()
        tableView.reloadData()
        self.navigationItem.title = String(format: "%.2f", DataManager.totalExpensesForPeriod(self.period))
    }
    
    func reloadExpensesData() {
        let context = CoreDataManager.masterContext()
        
        context?.performAndWait {
            do {
                try frcExpenses.performFetch()
            } catch {
                fatalError("Failed to fetch entities: \(error)")
            }
        }
    }
    
}
