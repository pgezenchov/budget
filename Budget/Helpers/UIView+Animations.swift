//
//  View+Animations.swift
//  Budget
//
//  Created by Petar Gezenchov on 27/09/2017.
//  Copyright © 2017 Petar Gezenchov. All rights reserved.
//

import Foundation

extension UIView {
    
    func fadeIn() {
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
    }
    
    func fadeOut() {
        UIView.animate(withDuration: 0.3) {
            self.alpha = 0
        }
    }
}
