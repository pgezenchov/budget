//
//  Date+Periods.swift
//  Budget
//
//  Created by Petar Gezenchov on 08/07/2018.
//  Copyright © 2018 Petar Gezenchov. All rights reserved.
//

import Foundation

extension Date {
    
    var day: Int {
        let calendar = Calendar.current
        return calendar.component(.day, from: self)
    }
    
    var month: Int {
        let calendar = Calendar.current
        return calendar.component(.month, from: self)
    }
    
    var year: Int {
        let calendar = Calendar.current
        return calendar.component(.year, from: self)
    }
    
    var dateAndMonthString: String {
        get {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd MMM yyyy"
            return formatter.string(from: self)
        }
    }
    
    var monthString: String {
        get {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM"
            return formatter.string(from: self)
        }
    }
    
    var yearString: String {
        get {
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy"
            return formatter.string(from: self)
        }
    }
    
    static func periodStartDateForDate(_ date: Date, periodStart: Int) -> Date {
        let calendar = Calendar.current
        var components = calendar.dateComponents([.year, .month, .day], from: date)
        
        // If day hasn't pass for the current month change to last month
        if components.day! < periodStart {
            components.month! -= 1
        }
        components.day = periodStart
        components.timeZone = TimeZone.current
        return calendar.date(from: components)!
    }
    
    static func aMonthFrom(_ from: Date) -> Date {
        var components = DateComponents()
        components.month = 1
        components.timeZone = TimeZone.current
        return Calendar.current.date(byAdding: components, to: from)!
    }
    
    static func aMonthBefore(_ from: Date) -> Date {
        var components = DateComponents()
        components.month = -1
        components.timeZone = TimeZone.current
        return Calendar.current.date(byAdding: components, to: from)!
    }
    
    static func endOfDay(_ day: Date) -> Date {
        var components = DateComponents()
        components.hour = 23
        components.minute = 59
        components.second = 59
        components.timeZone = TimeZone.current
        return Calendar.current.date(byAdding: components, to: day)!
    }

}
