//
//  PTGExpenseTransaction+CoreDataClass.swift
//  Budget
//
//  Created by Petar Gezenchov on 05/01/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//
//

import Foundation
import CoreData


public class PTGExpenseTransaction: PTGTransaction {

    static func createExpenseTransaction() -> PTGExpenseTransaction {
        return createExpenseTransaction(amount: 0, description: "no description", date: NSDate(), category: nil)
    }
    
    static func createExpenseTransaction(amount: Double, description: String, date: NSDate?, category: PTGExpenseGroup?) -> PTGExpenseTransaction {
        let expense = CoreDataManager.insertNewObjectForEntity(forName: "ExpenseTransaction", in: CoreDataManager.masterContext()) as! PTGExpenseTransaction
        
        expense.amount = amount
        expense.descriptionText = description
        expense.date = date
        expense.category = category
        // TODO: Safer access of date
        expense.sort = lastSortIndexForDay(date:date!.dateWithoutTime()! as NSDate)
        
        CoreDataManager.save()
        
        return expense
    }
    
    // TODO: Move to base class
    static func lastSortIndexForDay(date: NSDate) -> Int16 {
        do {
            let expenses = try CoreDataManager.masterContext().fetch(PTGExpenseTransaction.fetchRequestForDay(date: date))
            return Int16(expenses.count)
        } catch {
            
            fatalError("Failed to fetch category: \(error)")
        }
        return 0
    }
    
}
