//
//  PTGIncomeTransaction+CoreDataClass.swift
//  Budget
//
//  Created by Petar Gezenchov on 05/01/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//
//

import Foundation
import CoreData


public class PTGIncomeTransaction: PTGTransaction {
    
    static func createIncomeTransaction() -> PTGIncomeTransaction {
        return createIncomeTransaction(amount: 0, description: "no description", date: NSDate(), source: nil)
    }
    
    static func createIncomeTransaction(amount: Double, description: String, date: NSDate?, source: PTGIncomeGroup?) -> PTGIncomeTransaction {
        let income = CoreDataManager.insertNewObjectForEntity(forName: "IncomeTransaction", in: CoreDataManager.masterContext()) as! PTGIncomeTransaction
        
        income.amount = amount
        income.descriptionText = description
        income.date = date
        income.source = source
        // TODO: Safer access of date
        income.sort = lastSortIndexForDay(date:date!.dateWithoutTime()! as NSDate)
        
        CoreDataManager.save()
        
        return income
    }
    
    static func lastSortIndexForDay(date: NSDate) -> Int16 {
        do {
            let incomes = try CoreDataManager.masterContext().fetch(PTGIncomeTransaction.fetchRequestForDay(date: date))
            return Int16(incomes.count)
        } catch {
            
            fatalError("Failed to fetch category: \(error)")
        }
        return 0
    }
    
}
