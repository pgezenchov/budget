//
//  NSDateC.m
//  BFantastic
//
//  Created by Petar Gezenchov on 15/03/2015.
//  Copyright (c) 2015 com.firstonlinesolutions. All rights reserved.
//

#import "NSDate+Helpers.h"

@implementation NSDate (Helpers)

+ (NSDate *)fromExpenseDateString:(NSString *)expenseDateString {
    NSDateFormatter *dateFormatter  = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd MMM yyyy, H:mm:ss";
    
    return [dateFormatter dateFromString:expenseDateString];
}

- (NSDate *)dateWithoutTime {
    NSDateComponents *comps = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|
                               NSCalendarUnitMonth|
                               NSCalendarUnitDay|
                               NSCalendarUnitWeekOfMonth|
                               NSCalendarUnitWeekday|
                               NSCalendarUnitWeekdayOrdinal|
                               NSCalendarUnitQuarter|
                               NSCalendarUnitWeekOfMonth|
                               NSCalendarUnitWeekOfYear
                                                              fromDate:self];
    [comps setTimeZone:[NSTimeZone localTimeZone]];
    
    return [[NSCalendar currentCalendar] dateFromComponents:comps];
}

- (NSString*)localTimeString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd' 'HH:mm";
    
    NSTimeZone *local = [NSTimeZone localTimeZone];
    [dateFormatter setTimeZone:local];
    return [dateFormatter stringFromDate:self];
}

@end
