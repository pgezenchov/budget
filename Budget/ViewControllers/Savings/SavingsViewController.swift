//
//  SavingsViewController.swift
//  Budget
//
//  Created by Petar Gezenchov on 5/01/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//

import UIKit

class SavingsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    private var frcIncomes: NSFetchedResultsController<PTGIncomeTransaction>!
    private var editingIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // TODO: Handle if context can't be unwrapped here and in AddExpenseViewController.swift frcCategories
        // TODO: Make frcExpenses not force unwrapped and handle all across code here and in AddExpenseViewController.swift frcCategories
        // TODO: Check if fetchRequest can be nil and handle case here and in AddExpenseViewController.swift frcCategories
        
        let context = CoreDataManager.masterContext()
        let fetchRequest = PTGIncomeTransaction.fetchRequestForAllIncomes(ascending: false)
        frcIncomes = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context!, sectionNameKeyPath: "date.dateWithoutTime", cacheName: nil)
        reloadIncomesData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadIncomesTable()
    }
    
    // MARK: UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // TODO: Handle if guard statements fail
        guard let sections = frcIncomes?.sections else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[section]
        return sectionInfo.numberOfObjects
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let frc = frcIncomes {
            return frc.sections!.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let sectionInfo = frcIncomes?.sections?[section] else {
            return nil
        }
        
        // Convert string section name to date
        let dateString = sectionInfo.name
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
        let dateFromString = dateFormatter.date(from: dateString)
        
        // Convert the date back to string with desired display format
        dateFormatter.dateFormat = "dd MMMM"
        let stringDate: String = dateFormatter.string(from: dateFromString!)
        
        return stringDate
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PTGExpenseTableViewCell", for: indexPath) as! ExpenseTableViewCell
        
        guard let object = frcIncomes?.object(at: indexPath) else {
            fatalError("Attempt to configure cell without a managed object")
        }
        
        // Configure the cell with data from the managed object.
        let income = object
        
        // TODO: check if some additional checks should be made for optional UI elements before setting their value
        cell.amountLabel?.text = income.amountString
        cell.descriptionTextLabel?.text = income.descriptionText
        cell.dateLabel?.text = income.dateString
        // TODO: Think how to share expense/income cell better
        cell.categoryLabel.text = income.source?.name ?? "unknown"
        
        return cell
    }
    
    // MARK: UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if sourceIndexPath.row != destinationIndexPath.row {
            guard let sections = frcIncomes?.sections else {
                fatalError("No sections in fetchedResultsController")
            }
            let sectionInfo = sections[sourceIndexPath.section]
            
            guard var objects = sectionInfo.objects else {
                fatalError("Attempt to configure cell without a managed object")
            }
            let expense = objects.remove(at: sourceIndexPath.row)
            objects.insert(expense, at: destinationIndexPath.row)
            for (index, element) in objects.enumerated() {
                (element as! PTGExpenseTransaction).sort = Int16(index)
            }
            CoreDataManager.save()
            reloadIncomesData()
        }
    }
    
    func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        if sourceIndexPath.section != proposedDestinationIndexPath.section {
            return sourceIndexPath
        }
        
        return proposedDestinationIndexPath
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            guard let object = self.frcIncomes?.object(at: indexPath) else {
                fatalError("Attempt to configure cell without a managed object")
            }
            
            // Check if last object in section will be deleted to remove section later
            let lastObjectInSection = self.frcIncomes.sections![indexPath.section].numberOfObjects == 1
            
            // Delete the row from the data source
            CoreDataManager.masterContext().delete(object)
            CoreDataManager.save()
            self.reloadIncomesData()
            
            // Remove the section if all records are removed
            if lastObjectInSection {
                tableView.deleteSections(IndexSet(integer: 0), with: .fade)
            }
            else {
                // Delete the row in table animated
                tableView.deleteRows(at: [indexPath as IndexPath], with: .fade)
            }
        }
        
        let edit = UITableViewRowAction(style: .default, title: "Edit") { (action, indexPath) in
            // edit item at indexPath
            // Show expense screen to edit the record
            self.editingIndexPath = indexPath
            self.performSegue(withIdentifier: "EditExpenseSegue", sender: nil)
        }
        edit.backgroundColor = UIColor.lightGray
        
        return [delete, edit]
    }
    
    // MARK: Segue methods
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "EditIncomeSegue") {
            if let indexPath = editingIndexPath {
                // edit item at indexPath
                guard let object = self.frcIncomes?.object(at: indexPath) else {
                    fatalError("Attempt to configure cell without a managed object")
                }
                let expenseViewController = segue.destination as! ExpenseViewController
                expenseViewController.editingTransaction = object
                expenseViewController.transactionType = .income
                editingIndexPath = nil
            }
        }
        else if (segue.identifier == "NewIncomeSegue") {
            let expenseViewController = segue.destination as! ExpenseViewController
            expenseViewController.transactionType = .income
        }
    }
    
    
    // MARK: IBActions
    
    @IBAction func editButtonPressed(_ sender: Any) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        (sender as! UIBarButtonItem).title = tableView.isEditing ? "Done" : "Edit"
    }
    
    @IBAction func rightButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "NewIncomeSegue", sender: nil)
    }
    
    // MARK: Private methods
    
    func reloadIncomesTable() {
        reloadIncomesData()
        tableView.reloadData()
        self.navigationItem.title = String(format: "%.2f", DataManager.totalSavings())
    }
    
    func reloadIncomesData() {
        let context = CoreDataManager.masterContext()
        
        context?.performAndWait {
            do {
                try frcIncomes.performFetch()
            } catch {
                fatalError("Failed to fetch entities: \(error)")
            }
        }
    }
    
}
