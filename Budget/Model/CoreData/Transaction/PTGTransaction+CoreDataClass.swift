//
//  PTGTransaction+CoreDataClass.swift
//  Budget
//
//  Created by Petar Gezenchov on 05/01/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//
//

import Foundation
import CoreData


public class PTGTransaction: NSManagedObject {
    
    var amountString: String {
        get {
            let formatter = NumberFormatter()
            formatter.decimalSeparator = NSLocale.current.decimalSeparator
            formatter.maximumFractionDigits = 2
            let amountNumber = NSNumber(value:amount)
            return formatter.string(from: amountNumber)!
        }
    }
    
    var dateString: String {
        get {
            let formatter = DateFormatter()
            formatter.dateStyle = .medium
            formatter.timeStyle = .medium
            return formatter.string(from: date! as Date)
        }
    }
    
}
