//
//  PTGExpenseGroup+CoreDataProperties.swift
//  Budget
//
//  Created by Petar Gezenchov on 05/01/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//
//

import Foundation
import CoreData


extension PTGExpenseGroup {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PTGExpenseGroup> {
        return NSFetchRequest<PTGExpenseGroup>(entityName: "ExpenseGroup")
    }

    @NSManaged public var expenses: NSSet?

}

// MARK: Generated accessors for expenses
extension PTGExpenseGroup {

    @objc(addExpensesObject:)
    @NSManaged public func addToExpenses(_ value: PTGExpenseTransaction)

    @objc(removeExpensesObject:)
    @NSManaged public func removeFromExpenses(_ value: PTGExpenseTransaction)

    @objc(addExpenses:)
    @NSManaged public func addToExpenses(_ values: NSSet)

    @objc(removeExpenses:)
    @NSManaged public func removeFromExpenses(_ values: NSSet)

}
