//
//  ConfigurationManager.swift
//  Budget
//
//  Created by Petar Gezenchov on 10/02/2018.
//  Copyright © 2018 Petar Gezenchov. All rights reserved.
//

import UIKit

class ConfigurationManager: NSObject {
    static var periodStartDay: Int16 {
        get {
            // Return start day if already read from plist
            if let aConfiguredPeriodStartDay = ConfigurationManager.configuredPeriodStartDay {
                return aConfiguredPeriodStartDay
            }
            
            // Read configured value from plist
            guard let path = Bundle.main.path(forResource: "Configuration", ofType: "plist"),
                let dictionary = NSDictionary(contentsOfFile: path) as? [String: Any],
                let startDay = dictionary["periodStartDay"] as! Int16? else {
                    ConfigurationManager.configuredPeriodStartDay = defaultPeriodStartDay
                    return ConfigurationManager.configuredPeriodStartDay!
                }
            
            // Set in private variable so it's read only once
            ConfigurationManager.configuredPeriodStartDay = startDay
            return ConfigurationManager.configuredPeriodStartDay!
        }
    }
    private static var defaultPeriodStartDay:Int16 = 1
    private static var configuredPeriodStartDay:Int16?
}
