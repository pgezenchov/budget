//
//  UIView+Helpers.m
//  BFantastic
//
//  Created by Adrian on 3/31/15.
//  Copyright (c) 2015 com.firstonlinesolutions. All rights reserved.
//

#import "UIView+Helpers.h"

@implementation UIView (Helpers)

- (void)gradientWithStartColor:(UIColor*)startColor andEndColor:(UIColor*) endColor
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.colors = [NSArray arrayWithObjects:(id)startColor.CGColor, (id)endColor.CGColor, nil];
    gradient.frame = self.bounds;
    
    for (CALayer *layer in [self.layer sublayers]) {
        if ([layer isKindOfClass:[CAGradientLayer class]]) {
            [self.layer replaceSublayer:layer with:gradient];
            return;
        }
    }
    
    [self.layer addSublayer:gradient];
}

- (void)addBorder:(FOSUIViewBorder)border {
    return [self addBorder:border color:[UIColor blackColor]];
}

- (void)addBorder:(FOSUIViewBorder)border color:(UIColor*)color
{
    self.clipsToBounds = YES;
    
    CALayer *cborder = [CALayer layer];
    cborder.backgroundColor = color.CGColor;
    
    if((border & FOSUIViewBorderLeft) == FOSUIViewBorderLeft){
        CALayer *leftLayer = [CALayer layer];
        leftLayer.backgroundColor = cborder.backgroundColor;
        leftLayer.frame = CGRectMake(0, 0, 0.5f, CGRectGetHeight(self.frame));
        [self.layer addSublayer:leftLayer];
    }
    if((border & FOSUIViewBorderRight) == FOSUIViewBorderRight){
        CALayer *rightLayer = [CALayer layer];
        rightLayer.backgroundColor = cborder.backgroundColor;
        rightLayer.frame = CGRectMake(CGRectGetWidth(self.frame)-0.5f, 0, 0.5f, CGRectGetHeight(self.frame));
        [self.layer addSublayer:rightLayer];
    }
    if((border & FOSUIViewBorderTop) == FOSUIViewBorderTop){
        CALayer *topLayer = [CALayer layer];
        topLayer.backgroundColor = cborder.backgroundColor;
        topLayer.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), 0.5f);
        [self.layer addSublayer:topLayer];
    }
    if((border & FOSUIViewBorderBottom) == FOSUIViewBorderBottom){
        CALayer *bottomLayer = [CALayer layer];
        bottomLayer.backgroundColor = cborder.backgroundColor;
        bottomLayer.frame = CGRectMake(0, CGRectGetHeight(self.frame)-0.5f, CGRectGetWidth(self.frame), 0.5f);
        [self.layer addSublayer:bottomLayer];
    }
    /*switch(border){
     case FOSUIViewBorderLeft:
     cborder.frame = CGRectMake(0, 0, 1, CGRectGetHeight(self.frame));
     break;
     case FOSUIViewBorderRight:
     cborder.frame = CGRectMake(CGRectGetWidth(self.frame)-1, 0, 1, CGRectGetHeight(self.frame));
     break;
     case FOSUIViewBorderTop:
     cborder.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), 1);
     break;
     case FOSUIViewBorderBottom:
     cborder.frame = CGRectMake(0, CGRectGetHeight(self.frame)-1, CGRectGetWidth(self.frame), 1);
     break;
     }*/
    //cborder.frame = CGRectMake(-1, -1, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame)+2);
    
    //[self.layer addSublayer:cborder];
}

+ (UIView *)getViewFromNibWithNibName:(NSString *)nibName
{
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:nibName
                                                      owner:self
                                                    options:nil];
    
    return nibViews[0];
}

- (void)addRoundedCornersWithColor:(UIColor *)color
                   withBorderWidth:(CGFloat)width
                  withCornerRadius:(CGFloat)radius
{
    self.layer.borderColor      = color.CGColor;
    self.layer.cornerRadius     = radius;
    self.layer.borderWidth      = width;
    self.clipsToBounds          = YES;
}

- (void)shake
{
    static NSUInteger shakes = 0;
    static NSInteger direction = 1;
    [UIView animateWithDuration:0.1f animations:^
     {
         self.transform = CGAffineTransformMakeTranslation(5*direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 3)
         {
             shakes = 0;
             direction = 1;
             self.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake];
     }];
}

- (id)findFirstResponder
{
    if (self.isFirstResponder) {
        return self;
    }
    for (UIView *subView in self.subviews) {
        id responder = [subView findFirstResponder];
        if (responder)
            return responder;
    }
    return nil;
}

@end
