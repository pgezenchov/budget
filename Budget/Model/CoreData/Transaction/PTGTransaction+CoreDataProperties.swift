//
//  PTGTransaction+CoreDataProperties.swift
//  Budget
//
//  Created by Petar Gezenchov on 05/01/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//
//

import Foundation
import CoreData


extension PTGTransaction {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<PTGTransaction> {
        return NSFetchRequest<PTGTransaction>(entityName: "Transaction")
    }

    @NSManaged public var amount: Double
    public var date: NSDate? {
        set {
            // TODO: Make it more safe
            let newDate =  newValue!.dateWithoutTime()! as NSDate
            willChangeValue(forKey: "date")
            setPrimitiveValue(newDate, forKey: "date")
            didChangeValue(forKey: "date")
            
            self.sort = PTGExpenseTransaction.lastSortIndexForDay(date: newDate)
        }
        get {
            let date = primitiveValue(forKey: "date") as? NSDate
            return date
        }
    }
    @NSManaged public var descriptionText: String?
    @NSManaged public var sort: Int16

}
