//
//  PTGExpenseGroup+CoreDataClass.swift
//  Budget
//
//  Created by Petar Gezenchov on 05/01/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//
//

import Foundation
import CoreData


public class PTGExpenseGroup: PTGGroup {

    static func createExpenseGroup(name: String, sort: Int16) -> PTGExpenseGroup {
        let expenseGroup = CoreDataManager.insertNewObjectForEntity(forName: "ExpenseGroup", in: CoreDataManager.masterContext()) as! PTGExpenseGroup
        
        expenseGroup.name = name
        expenseGroup.sort = sort
        
        CoreDataManager.save()
        
        return expenseGroup
    }
    
}
