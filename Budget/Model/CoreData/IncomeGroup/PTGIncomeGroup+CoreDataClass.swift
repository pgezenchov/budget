//
//  PTGIncomeGroup+CoreDataClass.swift
//  Budget
//
//  Created by Petar Gezenchov on 05/01/2019.
//  Copyright © 2019 Petar Gezenchov. All rights reserved.
//
//

import Foundation
import CoreData


public class PTGIncomeGroup: PTGGroup {
    
    static func createIncomeGroup(name: String, sort: Int16) -> PTGIncomeGroup {
        let incomeGroup = CoreDataManager.insertNewObjectForEntity(forName: "IncomeGroup", in: CoreDataManager.masterContext()) as! PTGIncomeGroup
        
        incomeGroup.name = name
        incomeGroup.sort = sort
        
        CoreDataManager.save()
        
        return incomeGroup
    }
    
}
